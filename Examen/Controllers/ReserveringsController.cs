﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;
using System.IO;
using IronPdf;

namespace Examen.Controllers
{
    [Authorize]
    public class ReserveringsController : Controller
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: Reserverings
        public async Task<ActionResult> Index()
        {
            var reservering = db.Reservering.Include(r => r.Klant).Where(x => x.Datum == DateTime.Today).Where(x => x.IsActive == true).ToList();
            return View(reservering);
        }

        // GET: Reserverings/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservering reservering = await db.Reservering.FindAsync(id);
            if (reservering == null)
            {
                return HttpNotFound();
            }
            return View(reservering);
        }

        // GET: Reserverings/Create
        public ActionResult Create()
        {
            ViewBag.KlantID = new SelectList(db.Klant, "ID", "Naam");
            ViewBag.Alert = 0;
            return View();
        }

        // POST: Reserverings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Datum,Tijd,Tafel,AantalPersonen,Status,DatumToegevoegd,BonDatum,Betalingswijze,KlantID")] Reservering reservering)
        {
            if (ModelState.IsValid)
            {
                reservering.Afgerond = false;
                int count = db.Reservering.Where(x => x.KlantID == reservering.KlantID && x.Status == false).Count();
                string data = "";
                if (count > 0)
                {
                    TempData[data] = count.ToString() ;
                }
                reservering.IsActive = true;
                reservering.DatumToegevoegd = DateTime.Now;
                db.Reservering.Add(reservering);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.KlantID = new SelectList(db.Klant, "ID", "Naam", reservering.KlantID);
            return View(reservering);
        }

        // GET: Reserverings/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservering reservering = await db.Reservering.FindAsync(id);
            if (reservering == null)
            {
                return HttpNotFound();
            }
            ViewBag.KlantID = new SelectList(db.Klant, "ID", "Naam", reservering.KlantID);
            return View(reservering);
        }

        // POST: Reserverings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Datum,Tijd,Tafel,AantalPersonen,Status,DatumToegevoegd,BonDatum,Betalingswijze,KlantID")] Reservering reservering)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservering).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.KlantID = new SelectList(db.Klant, "ID", "Naam", reservering.KlantID);
            return View(reservering);
        }

        // GET: Reserverings/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservering reservering = await db.Reservering.FindAsync(id);
            if (reservering == null)
            {
                return HttpNotFound();
            }
            return View(reservering);
        }

        // POST: Reserverings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Reservering reservering = await db.Reservering.FindAsync(id);
            reservering.IsActive = false;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // Hier word de bon gemaakt om vervolgens uit te kunnen printen. hij word eerst omgezet naar pdf en daarna kan je hem printen.
        public ActionResult Bon(int? id)
        {
            if (id == null)
            {
                return (RedirectToAction("index"));
            }
            else
            {
                var bestellingen = db.Bestelling.Where(x => x.ReserveringID == id).ToList();
                ViewBag.Table = db.Reservering.Where(x => x.ID == id).Select(x => x.Tafel).FirstOrDefault();
                ViewBag.ID = id;
                return View(bestellingen);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
