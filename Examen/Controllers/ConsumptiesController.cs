﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    [Authorize]
    public class ConsumptiesController : Controller
    {
        // Database gebruiken
        private DatabaseEntities db = new DatabaseEntities();

        // Haal de consumpties op.
        public async Task<ActionResult> Index()
        {
            return View(await db.Consumptie.ToListAsync());
        }
        public async Task<ActionResult> Dranken()
        {
            return View(await db.Consumptie.ToListAsync());
        }
        public async Task<ActionResult> Gerechten()
        {
            return View(await db.Consumptie.ToListAsync());
        }

        // haal de details op van de consumpties.
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumptie consumptie = await db.Consumptie.FindAsync(id);
            if (consumptie == null)
            {
                return HttpNotFound();
            }
            return View(consumptie);
        }

        // GET: Consumpties/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Consumpties/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Code,Naam")] Consumptie consumptie)
        {
            if (ModelState.IsValid)
            {
                var find = db.Consumptie.Where(x => x.Code == consumptie.Code).Count();
                if (find > 0)
                {
                    return View(consumptie);
                }
                db.Consumptie.Add(consumptie);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(consumptie);
        }

        // GET: Consumpties/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumptie consumptie = await db.Consumptie.FindAsync(id);
            if (consumptie == null)
            {
                return HttpNotFound();
            }
            return View(consumptie);
        }

        // POST: Consumpties/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Code,Naam")] Consumptie consumptie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consumptie).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(consumptie);
        }

        // GET: Consumpties/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumptie consumptie = await db.Consumptie.FindAsync(id);
            if (consumptie == null)
            {
                return HttpNotFound();
            }
            return View(consumptie);
        }

        // POST: Consumpties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Consumptie consumptie = await db.Consumptie.FindAsync(id);
            db.Consumptie.Remove(consumptie);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
