﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    [Authorize]
    public class ConsumptieGroepsController : Controller
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: ConsumptieGroeps
        public async Task<ActionResult> Index()
        {
            var consumptieGroep = db.ConsumptieGroep.Include(c => c.Consumptie);
            return View(await consumptieGroep.ToListAsync());
        }
        // GET: ConsumptieGroeps/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieGroep consumptieGroep = await db.ConsumptieGroep.FindAsync(id);
            if (consumptieGroep == null)
            {
                return HttpNotFound();
            }
            return View(consumptieGroep);
        }

        // GET: ConsumptieGroeps/Create
        public ActionResult Create()
        {
            ViewBag.ConsumptieCode = new SelectList(db.Consumptie, "Code", "Naam");
            return View();
        }

        // POST: ConsumptieGroeps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Code,Naam,ConsumptieCode")] ConsumptieGroep consumptieGroep)
        {
            if (ModelState.IsValid)
            {
                var find = db.ConsumptieGroep.Where(x => x.Code == consumptieGroep.Code).Count();
                if (find > 0)
                {
                    ViewBag.ConsumptieCode = new SelectList(db.Consumptie, "Code", "Naam");
                    return View(consumptieGroep);
                }
                db.ConsumptieGroep.Add(consumptieGroep);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ConsumptieCode = new SelectList(db.Consumptie, "Code", "Naam", consumptieGroep.ConsumptieCode);
            return View(consumptieGroep);
        }

        // GET: ConsumptieGroeps/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieGroep consumptieGroep = await db.ConsumptieGroep.FindAsync(id);
            if (consumptieGroep == null)
            {
                return HttpNotFound();
            }
            ViewBag.ConsumptieCode = new SelectList(db.Consumptie, "Code", "Naam", consumptieGroep.ConsumptieCode);
            return View(consumptieGroep);
        }

        // POST: ConsumptieGroeps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Code,Naam,ConsumptieCode")] ConsumptieGroep consumptieGroep)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consumptieGroep).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ConsumptieCode = new SelectList(db.Consumptie, "Code", "Naam", consumptieGroep.ConsumptieCode);
            return View(consumptieGroep);
        }

        // GET: ConsumptieGroeps/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieGroep consumptieGroep = await db.ConsumptieGroep.FindAsync(id);
            if (consumptieGroep == null)
            {
                return HttpNotFound();
            }
            return View(consumptieGroep);
        }

        // POST: ConsumptieGroeps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ConsumptieGroep consumptieGroep = await db.ConsumptieGroep.FindAsync(id);
            db.ConsumptieGroep.Remove(consumptieGroep);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
