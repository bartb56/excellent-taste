﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    [Authorize]
    public class BestellingsController : Controller
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: Bestellingen    
        public async Task<ActionResult> Index()
        {
            var bestelling = db.Bestelling.Include(b => b.ConsumptieItem).Include(b => b.Reservering);
            return View(await bestelling.ToListAsync());
        }

        // GET: Bestellings/Details
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return HttpNotFound();
            }
            return View(bestelling);
        }

        // GET: Bestellingen/Create Maak een bestelling aan.
        public ActionResult Create()
        {
            ViewBag.ConsumptieItemCode = new SelectList(db.ConsumptieItem, "Code", "Naam");
            List<Reservering> Reserveringen = db.Reservering.Where(x => x.Afgerond == false && x.IsActive == true).ToList();
            ViewBag.ReserveringID = new SelectList(Reserveringen, "ID", "Tafel");
            return View();
        }

        // POST: Bestellings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Aantal,DateTimeBereidingConsumptie,ConsumptieItemCode,ReserveringID,Prijs")] Bestelling bestelling)
        {
            if (ModelState.IsValid)
            {
                bestelling.Afgerond = false;
                var price = db.ConsumptieItem.First(x => x.Code == bestelling.ConsumptieItemCode);
                bestelling.Prijs = bestelling.Aantal * price.Prijs;
                bestelling.DateTimeBereidingConsumptie = DateTime.Now;
                db.Bestelling.Add(bestelling);
                await db.SaveChangesAsync();
                return RedirectToAction("CREATE");
            }

            ViewBag.ConsumptieItemCode = new SelectList(db.ConsumptieItem, "Code", "Naam", bestelling.ConsumptieItemCode);
            ViewBag.ReserveringID = new SelectList(db.Reservering, "ID", "Betalingswijze", bestelling.ReserveringID);
            return View(bestelling);
        }

        // GET: Bestellings/Edit
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return HttpNotFound();
            }
            ViewBag.ConsumptieItemCode = new SelectList(db.ConsumptieItem, "Code", "Naam", bestelling.ConsumptieItemCode);
            ViewBag.ReserveringID = new SelectList(db.Reservering, "ID", "Betalingswijze", bestelling.ReserveringID);
            return View(bestelling);
        }

        // POST: Bestellings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Aantal,DateTimeBereidingConsumptie,ConsumptieItemCode,ReserveringID,Prijs")] Bestelling bestelling)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bestelling).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ConsumptieItemCode = new SelectList(db.ConsumptieItem, "Code", "Naam", bestelling.ConsumptieItemCode);
            ViewBag.ReserveringID = new SelectList(db.Reservering, "ID", "Betalingswijze", bestelling.ReserveringID);
            return View(bestelling);
        }

        // GET: Bestellings/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            if (bestelling == null)
            {
                return HttpNotFound();
            }
            return View(bestelling);
        }

        // POST: Bestellings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Bestelling bestelling = await db.Bestelling.FindAsync(id);
            db.Bestelling.Remove(bestelling);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
