﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen.Models;

namespace Examen.Controllers
{
    [Authorize]
    public class ConsumptieItemsController : Controller
    {
        private DatabaseEntities db = new DatabaseEntities();

        public ActionResult Index()
        {
            var Bestellen = db.ConsumptieItem.Where(x => x.IsActive == true).OrderBy(x => x.ConsumptieGroepCode).ToList();
            return View(Bestellen);
        }
        public ActionResult MenuKaart()
        {
            var menu = db.ConsumptieItem.Where(x => x.IsActive == true).OrderBy(x => x.ConsumptieGroep.Consumptie.Naam).ToList();
            return View(menu);
        }

        // GET: ConsumptieItems
        public ActionResult Barman()
        {
            var Bestellen = db.Bestelling.Where(x => x.ConsumptieItem.ConsumptieGroep.Consumptie.Code == "dr" && x.Afgerond == false).OrderBy(x => x.ReserveringID);
            return View(Bestellen.ToList());
        }
        public ActionResult ChefKok()
        {
            var Bestellen = db.Bestelling.Where(x => x.ConsumptieItem.ConsumptieGroep.Consumptie.Code == "ha" && x.Afgerond == false).OrderBy(x => x.ReserveringID);
            return View(Bestellen.ToList());
        }
        // GET: ConsumptieItems/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieItem consumptieItem = await db.ConsumptieItem.FindAsync(id);
            if (consumptieItem == null)
            {
                return HttpNotFound();
            }
            return View(consumptieItem);
        }

        // GET: ConsumptieItems/Create
        public ActionResult Create()
        {
            ViewBag.ConsumptieGroepCode = new SelectList(db.ConsumptieGroep, "Code", "Code");
            return View();
        }

        // POST: ConsumptieItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Code,Naam,Prijs,ConsumptieGroepCode")] ConsumptieItem consumptieItem)
        {
            if (ModelState.IsValid)
            {
                var find = db.ConsumptieItem.Where(x => x.Code == consumptieItem.Code).Count();
                if (find > 0)
                {
                    ViewBag.ConsumptieGroepCode = new SelectList(db.ConsumptieGroep, "Code", "Code");
                    return View(consumptieItem);
                }
                consumptieItem.IsActive = true;
                db.ConsumptieItem.Add(consumptieItem);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ConsumptieGroepCode = new SelectList(db.ConsumptieGroep, "Code", "Naam", consumptieItem.ConsumptieGroepCode);
            return View(consumptieItem);
        }

        // GET: ConsumptieItems/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieItem consumptieItem = await db.ConsumptieItem.FindAsync(id);
            if (consumptieItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.ConsumptieGroepCode = new SelectList(db.ConsumptieGroep, "Code", "Naam", consumptieItem.ConsumptieGroepCode);
            return View(consumptieItem);
        }

        // POST: ConsumptieItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Code,Naam,Prijs,ConsumptieGroepCode")] ConsumptieItem consumptieItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consumptieItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ConsumptieGroepCode = new SelectList(db.ConsumptieGroep, "Code", "Naam", consumptieItem.ConsumptieGroepCode);
            return View(consumptieItem);
        }

        // GET: ConsumptieItems/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConsumptieItem consumptieItem = await db.ConsumptieItem.FindAsync(id);
            if (consumptieItem == null)
            {
                return HttpNotFound();
            }
            return View(consumptieItem);
        }

        // POST: ConsumptieItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ConsumptieItem consumptieItem = await db.ConsumptieItem.FindAsync(id);
            consumptieItem.IsActive = false;

            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
